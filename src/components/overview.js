import React from 'react';
import CardSmall from './card-small'
import '../css/overview.css';

const card_small = [
  {
    id: 1,
    icon: 'images/icon-facebook.svg',
    pageViews: '87',
    growth: 100,
  },
  {
    id: 2,
    icon: 'images/icon-twitter.svg',
    pageViews: '90',
    growth: 200,
  },
  {
    id: 3,
    icon: 'images/icon-instagram.svg',
    pageViews: '100',
    growth: 300,
  },
  {
    id: 4,
    icon: 'images/icon-youtube.svg',
    pageViews: '200',
    growth: 400,
  },
]

export default function Overview() {
  return (
    <section className="overview">
    <div className="wrapper">
      <h2>Overview - Today</h2>
      <div className="grid">
        {
          card_small.map(({ id, icon, pageViews, growth }) =>
            <CardSmall
              key={id}
              icon={ icon }
              pageViews={ pageViews }
              growth={ growth }
            />
          )
        }
      </div>
    </div>
  </section>
  )
}
