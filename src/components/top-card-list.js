import React from 'react';
import Card from './card';
import '../css/top-card-list.css';

const card_list_data = [
  {
    id: 1,
    username: "@XavierHiDo",
    followers: '1483',
    today_followers: 12,
    icon: 'images/icon-facebook.svg',
    name: 'facebook'
  },
  {
    id: 2,
    username: "@XavierHiDo",
    followers: '28k',
    today_followers: 100,
    icon: 'images/icon-twitter.svg',
    name: 'twitter'
  },
  {
    id: 3,
    username: "@XavierHiDo",
    followers: '6k',
    today_followers: 500,
    icon: 'images/icon-instagram.svg',
    name: 'instagram'
  },
  {
    id: 4,
    username: "@XavierHiDo",
    followers: '12k',
    today_followers: 2000,
    icon: 'images/icon-youtube.svg',
    name: 'youtube'
  },
]

function TopCardList() {
  return (
    <section className="top-cards">
      <div className="wrapper">
        <div className="grid">
          {
            card_list_data.map(({ id, username, followers, today_followers, icon, name }) =>
              <Card
                key={id}
                username={ username }
                followers={ followers }
                today_followers={ today_followers }
                icon={ icon }
                name={ name }
                />
            )
            // card_list_data.map((card_data) =>  <Card key={card_data.id} { ...card_data } /> )
          }
        </div>
      </div>
    </section>
  )
}

export default TopCardList;
