import React, { Fragment, useState, useEffect } from "react";
import Header from "./components/header";
import TopCardList from "./components/top-card-list";
import Overview from "./components/overview";
import Switch from "./components/switch";
import "./css/globals.css";

function App() {
  const [dark_mode, set_dark_mode] = useState(false);
  const [checked, set_checked] = useState(false);

  const main_class = dark_mode ? "is-dark-mode" : "is-light-mode";

  function changeMedia(mq) {
    set_dark_mode(mq.matches);
    set_checked(mq.matches);
  }

  useEffect(() => {
    const mq = window.matchMedia("(prefers-color-scheme: dark)");
    mq.addListener(changeMedia);
    set_dark_mode(mq.matches);
    set_checked(mq.matches);
  }, []);

  return (
    // <Fragment>
    <main className={main_class}>
      <Header>
        <Switch
          set_dark_mode={set_dark_mode}
          checked={checked}
          set_checked={set_checked}
        />
      </Header>
      <TopCardList />
      <Overview />
    </main>
    // </Fragment>
  );
}

export default App;
